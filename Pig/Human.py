from PrintInstructions import PrintGameInstructions
from Score import ScoreClass
from Die import *
from ComputerController import *
from GameUmpire import *

def HumanTurn():

    rollAgainDecision = True
    newRollNumber = 0
    humanScoreList = []
    humanScoreListSum = 0
    addNewScore = ScoreClass(0, 0)

    while(rollAgainDecision):

        rollAgainDecision = ClassDieControl().RollAgain()

        if(rollAgainDecision):

            newRollNumber = ClassDieControl().Roll()
            print("newRollNumber: ", newRollNumber)
            print("Your roll value: ", newRollNumber)

            if(newRollNumber == 1):

                humanScoreList = []
                humanScoreListSum = 0
                print("You rolled a 1. You lose your turn and your turn score.")
                ComputerTurn()

            else:
                
                humanScoreList.append(newRollNumber)
                humanScoreListSum = sum(humanScoreList)
                print("New sum for this turn: ", humanScoreListSum)

        else:

            addNewScore.CalculateHumanScore(humanScoreListSum)
            humanScoreListSum = 0
            humanScoreList = []
            computerScore = ComputerTurn()
            addNewScore.CalculateComputerScore(computerScore)
            rollAgainDecision = True

        print("Human score: ", addNewScore.humanScore)
        print("Computer score: ", addNewScore.computerScore)
        gameOver = IsGameOver(addNewScore.humanScore, addNewScore.computerScore)

        if(gameOver):
            break
