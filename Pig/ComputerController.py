import random
from Die import *

def ComputerTurn():

    rollAgainDecision = True ## gets program inside while loop
    newRollNumber = 0
    computerScoreList = []
    computerScoreListSum = 0
    
    while(rollAgainDecision):

        rollAgainDecision = False
        computerRollDecision = random.randrange(1, 3, 1) ## gets program inside if statement

        if(computerRollDecision == 1):

            rollAgainDecision = True ## keeps program inside while loop
            computerRoll = ClassDieControl().Roll()
            print("Computer wants to roll.")

            if(computerRoll == 1):

                rollAgainDecision = False ## rolled a 1. Kicks program out of while loop
                computerScoreListSum = 0
                print("Computer rolled a 1. Turn is over.")
                return computerScoreListSum

            else:

                computerScoreList.append(computerRoll)
                computerScoreListSum = sum(computerScoreList)
                computerRollDecision = random.randrange(1, 3, 1)
                print("Computer rolled a ", computerRoll, ".")
                print("Computer score sum is ", computerScoreListSum, ".")

        else:

            print("Computer doesn't want to roll.")
            return computerScoreListSum
